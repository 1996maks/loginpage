import { Component, OnInit } from '@angular/core';
import { Routes, Router} from '@angular/router';
import {TranslateService} from 'ng2-translate';
import { Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {Http} from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  name : string;
  logout = false;
  userName = false;
  title = 'app';
  constructor(private route: Router, public translate: TranslateService, private http: Http){
    this.translate.addLangs(['en', 'rus']);
    this.translate.setDefaultLang('en');

    const browserLang = this.translate.getBrowserLang();
    this.translate.use(browserLang.match(/en|rus/) ? browserLang : 'en');
  }
  ngOnInit(){
    if(localStorage.getItem('name') != null){
      this.name = localStorage.getItem('name');
      this.logout = true;
      this.userName = true;
    }
    else {
      this.logout = false;
      this.logout = false;
    }
  }
  logoutUser(){
    localStorage.removeItem('name');
    this.name = '';
    this.logout = false;
    this.route.navigate(['login']);
  }
  setLang(language) {
    localStorage.setItem('language', language);
    this.translate.use(language);
  }
}
