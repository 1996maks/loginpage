import { Component, OnInit } from '@angular/core';
import { Routes, Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  name : string;
  password : string;
  constructor(private route : Router) { }

  ngOnInit() {
  }

  loginUser(){
    console.log(this.name + '+' + this.password);
    localStorage.setItem('name', this.name);
    this.route.navigate(['home']);
    window.location.reload();
  }
}
